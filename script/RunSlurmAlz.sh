#  !bash

llaf_dir=`cd ..;pwd`
if [ ! -n $1 ]; then
slurm_dir=/home/liwen/software/slurm-slurm-15-08-7-1
else
slurm_dir=$1
fi

cd $llaf_dir/build/bin
name=buildCg
tool=./$name

PreFile=$llaf_dir/build/bin/Func_Module.map
if [ ! -f $PreFile ]; then
$tool -dir $slurm_dir -pre 1
fi

bc_set=`find $slurm_dir -name s*.preopt.bc`
function GetProNum()
{
    ProSet=`ps -ef | grep $name | grep -v grep | awk '{print $2}'`
	if [ ! -n "$ProSet" ]; then	
		return 0
	fi

	echo ProSet=$ProSet
	ProNum=0
	for Id in $ProSet
	{
		ProNum=$[ $ProNum + 1 ]
	}
	
	echo ProNum=$ProNum
	return $ProNum
}


for bc in $bc_set
do
	while true
	do
		GetProNum
		if [ "$?" -lt "4" ]; then 
			echo "run analysis on slurm[$bc]"
			$tool -file $bc	> $bc.result &
			break
		else
			sleep 100
		fi
	done
done

