#  !bash

oaf_dir=`cd ..;pwd`
slurm_dir=/home/liwen/software/slurm-slurm-15-08-7-1

cd $oaf_dir/build/bin
name=Oaf
tool=./$name

bc_set=`find $slurm_dir -name s*.opt.bc`

for bc in $bc_set
do
	file=${bc##*/}
	hw_file=${file%%.*}
	
	$tool -file $bc	-dump-CFG-weight 1
	
	mv "CFG_heavyweight" $hw_file"_CFG_heavyweight".txt
	
done

