#  !bash


TOOL=$TOOL_PATH/MemCheck

#compile
SOURCE_SET=`ls *.c`
for srouce in $SOURCE_SET
do
	clang -g -c -emit-llvm $srouce -o $srouce.bc
done

return;
#start run all case
CASE_SET=`ls *.bc`
echo $CASE_SET
for case in $CASE_SET
do
	echo
	echo "=====> start analysis on" $case 
	$TOOL -file $case -memleak-test 1
done
