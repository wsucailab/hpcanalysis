#include "route.h"

typedef void* (*MALLOC) (int size);

void *MemMalloc (int size)
{
	void *Addr = malloc (size);
	if (Addr == NULL)
	{
		return NULL;
	}
	
	memset (Addr, 0, size);
	
	return Addr;
}

int *Operate (int Opt, MALLOC GetMemory)
{
	int *Mem = NULL;
	Mem = (int*)GetMemory (4);
	if (Mem == NULL)
	{
		return NULL;
	}
	
	*Mem = Opt*4;
	
	return Mem;
}

int main(int argc, char * argv[])
{	
	int Opt = atoi (argv[1]);
	int *Res = 0;
	
	MALLOC GetMem = MemMalloc;
	
	Res = Operate (Opt, GetMem);
	if (Res == NULL)
	{
		return -1;
	}
	
	switch (Opt)
	{
		case 1:
		{	
		    printf("Opt1 \r\n");
		}
		case 2:
		{
			printf("Opt2 \r\n");
		    break;
		}
		default:
		{
			return -1;
		}
	}
	
	free (Res);
}
