#include "route.h"

static double** MemLeak (int Opt)
{
	int Num = 4;
	
	double **u = malloc (Num * sizeof(double*));
	if (u == NULL)
	{
		return NULL;
	}
	
	int i = 0;
	for (i = 0; i < Num; i++)
	{
	    u[i] = malloc (4 * sizeof(double));
		if (u[i] == NULL)
		{
			break;
		}
	}

	if (i < Num)
	{
		int j = i;
		for (i = 1; i < j; i++)
		{
			free(u[i]);
		}
		
		free (u);
		return NULL;
	}
	
	return u;
}

int main(int argc, char * argv[])
{	
	double **addr = MemLeak (1);
	if (addr == NULL)
	{
		return 0;
	}
	
	free (addr);
}
