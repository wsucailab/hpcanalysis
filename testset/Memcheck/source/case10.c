#include "route.h"

typedef void* (*MALLOC) (int size);

typedef struct
{
	int Opt;
	MALLOC GetMem;
	int *Res;
}T_Op;


void *MemMalloc (int size)
{
	void *Addr = malloc (size);
	if (Addr == NULL)
	{
		return NULL;
	}
	
	memset (Addr, 0, size);
	
	return Addr;
}

void Operate (T_Op* operator)
{
	int *Mem = NULL;
	Mem = operator->GetMem (4);
	if (Mem == NULL)
	{
		return;
	}
	
	int Opt = operator->Opt;
	*Mem = Opt*4;
	
	operator->Res = Mem;
	return;
}

int main(int argc, char * argv[])
{	
	int Para = atoi (argv[1]);
	T_Op operator = {Para, MemMalloc, NULL}; 
	
	Operate (&operator);
	if (operator.Res == NULL)
	{
		return -1;
	}
	
	free (operator.Res);
}
